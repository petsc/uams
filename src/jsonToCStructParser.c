#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ams-private/amsprivate.h>
#include <ams-private/cJSON.h>

/* Compares the dtype string and assigns the appropriate value to the field */
static int dtype_String_to_Enum(AMS_Field fld2,char *dtype)
{


  if(strcmp(dtype,"AMS_CHAR") == 0) {
    fld2->dtype = AMS_CHAR;
    return AMS_ERROR_NONE;
  }

  if(strcmp(dtype,"AMS_BOOLEAN") == 0) {
    fld2->dtype = AMS_BOOLEAN;
    return AMS_ERROR_NONE;
  }

  if(strcmp(dtype,"AMS_INT") == 0) {
    fld2->dtype = AMS_INT;
    return AMS_ERROR_NONE;
  }

  if(strcmp(dtype,"AMS_FLOAT") == 0) {
    fld2->dtype = AMS_FLOAT;
    return AMS_ERROR_NONE;
  }

  if(strcmp(dtype,"AMS_DOUBLE") == 0) {
    fld2->dtype = AMS_DOUBLE;
    return AMS_ERROR_NONE;
  }

  if(strcmp(dtype,"AMS_STRING") == 0) {
    fld2->dtype = AMS_STRING;
    return AMS_ERROR_NONE;
  }

  /* If the comparison is none of the above, default to UNDEF */
  fld2->dtype = AMS_DATA_UNDEF;
  return AMS_ERROR_NONE;
}


/* Compares the mtype string and assigns the appropriate value to the field */
static int mtype_String_to_Enum(AMS_Field fld2,char* mtype)
{

  if(strcmp(mtype,"AMS_READ") == 0) {
    fld2->mtype = AMS_READ;
    return AMS_ERROR_NONE;
  }

  if(strcmp(mtype,"AMS_WRITE") == 0) {
    fld2->mtype = AMS_WRITE;
    return AMS_ERROR_NONE;
  }

  /* If the comparison is none of the above, default to UNDEF */
  fld2->mtype = AMS_MEMORY_UNDEF;
  return AMS_ERROR_NONE;
}

/*checks to see if the string is only numeric; if not, returns an error code*/
static int isnumeric(char *data)
{
  int i;

  for(i=0;i<strlen(data);i++){
    if(!isdigit(data[i])){
      if(data[i] != '.'){
        return AMS_ERROR_NOT_NUMERIC;
      }
    }
  }

  return AMS_ERROR_NONE;

}


/* This function turns the data into a string, using AMS_Data_type to decide what case to use */
static int string_to_data(AMS_Field fld2,char *data_String,int counter)
{

  if(fld2->dtype == AMS_DATA_UNDEF){
    //return "AMS_DATA_UNDEF";
  }

  if(fld2->dtype == AMS_CHAR) {
    
    ((char*)fld2->data)[counter] = data_String[0];
    return AMS_ERROR_NONE;
  }

  if(fld2->dtype == AMS_DOUBLE) {

    if(isnumeric(data_String) == AMS_ERROR_NOT_NUMERIC){
      return AMS_ERROR_NOT_NUMERIC;
    } else {
      char   *pEnd;     
      ((double*)fld2->data)[counter] = strtod(data_String,&pEnd);
      return AMS_ERROR_NONE;
    }
  }

  if(fld2->dtype == AMS_FLOAT) {
    
    if(isnumeric(data_String) == AMS_ERROR_NOT_NUMERIC){
      return AMS_ERROR_NOT_NUMERIC;
    } else {
      char *pEnd;
      ((float*)fld2->data)[counter] = strtof(data_String,&pEnd);
      return AMS_ERROR_NONE;
    }
  }

  if(fld2->dtype == AMS_STRING) {

    free(((char**)fld2->data)[counter]);
    ((char**)fld2->data)[counter] = strdup(data_String);
    return AMS_ERROR_NONE;
  }

  if(fld2->dtype == AMS_INT) {
    
    if(isnumeric(data_String) == AMS_ERROR_NOT_NUMERIC){
      return AMS_ERROR_NOT_NUMERIC;
    } else {
      char *pEnd;
      ((int*)fld2->data)[counter] = strtol(data_String,&pEnd,10);
      return AMS_ERROR_NONE;
    }
  }

  if(fld2->dtype == AMS_BOOLEAN) {
    
    int i;
    
    /*convert to lower case*/
    for(i=0;i<strlen(data_String);i++){
      data_String[i] = tolower(data_String[i]);
    }
    
    if(strcmp(data_String,"true") == 0) {
      ((int*)fld2->data)[counter]   = 1;
    } else if(strcmp(data_String,"false") == 0) {
      ((int*)fld2->data)[counter]   = 0;
    } else {
      return AMS_ERROR_NOT_BOOLEAN;
    }
    return AMS_ERROR_NONE;
  }

  return AMS_ERROR_DATATYPE_NOT_FOUND;
}


/* Converts a JSON Serialized string (of a specific structure [memories]) to a memory */
int JSONParser_JSON_to_CStruct(char *JSON_Serialized_Object)
{

  char       *title_Of_Info;
  AMS_Memory amem = memoryList;

  /*Should find memories:*/
  title_Of_Info = strtok(JSON_Serialized_Object,"\":' ,\n}{[]");

  while (amem != NULL){
    
    JSONParser_Single_JSON_to_CStruct(JSON_Serialized_Object,amem->name,1);

    amem = amem->next;

  }

  return AMS_ERROR_NONE;
}

int find_Data_Tokens(char *titlePointer,AMS_Field fld,char *dataPointer)
{
  char *endDataPointer;
  char *currentPointer;
  
  endDataPointer = strstr(titlePointer,"],\"dtype");

  for(currentPointer = titlePointer;currentPointer < endDataPointer;currentPointer++){
    

    if(currentPointer == "'"){
      if(currentPointer == ","){
        /*proceed to next entry*/
      }
    }

  }
}

/* Converts a JSON Serialized string (of a specific structure [memories]) to a memory 
 * if we are being called from the function to do all memories, allFlag should be set to 1*/
int JSONParser_Single_JSON_to_CStruct(char *JSON_Serialized_Object,char *name,int allFlag)
{
  int       i, found_Memory_Flag = 0;
  char      *title_Of_Info,*name_Field_Info,*data_Info;
  char      *dtype_Info,*mtype_Info,*length_Info, *name_Memory_Info;
  /* declare a field */
  AMS_Field fld;
  void      *data;

  /*start on the first memory*/
  AMS_Memory amem = memoryList;

  /*Should find memories:*/
  // title_Of_Info = strtok(JSON_Serialized_Object,"\":' ,\n}{[]");

  /*Find the named memory*/
  while(amem != NULL){
    
    if(strcmp(amem->name, name) == 0){
      found_Memory_Flag = 1;
      break;
    }
    amem = amem->next;
  }

  if(found_Memory_Flag != 1) {
    return AMS_ERROR_MEMORY_NOT_FOUND;
  }

  /*Lock the memory*/
  AMS_Lock_Memory(amem);

  /*Go to the set of fields*/
  fld = amem->fields;


  /*call strtok to find all the fields and data*/
  /* this might not be the best solution */

  /*Should find name: and the name of the memory
   *if allFlag is set, then we are working of a strtok in the previous function*/
  if(allFlag){
    title_Of_Info = strtok(NULL,"\":' ,\n}{[]");
  } else {
    title_Of_Info = strtok(JSON_Serialized_Object, "\":' ,\n}{[]");
  }
  name_Memory_Info = strtok(NULL,"\":' ,\n}{[]");
  
  /*This should find fields:*/
  title_Of_Info = strtok(NULL,"\":' ,\n}{[]");
    
  while(fld->next != NULL){

    /*Should find name: and the name of the field*/
    title_Of_Info   = strtok(NULL,"\":' ,\n}{[]");
    name_Field_Info = strtok(NULL,"\":' ,\n}{[]");
    
    /*Should find data: and the data of the field*/
    title_Of_Info  = strtok(NULL,"\":' ,\n}{[]");

    find_Data_Tokens(title_Of_Info,fld,data_Info);
 
    for(i=0;i<fld->len;i++){
      data_Info    = strtok(NULL,"\":' ,\n}{[]");

      /*Only change the data if it is AMS_WRITE*/
      if(fld->mtype == AMS_WRITE){
        string_to_data(fld,data_Info,i);          
      }
    }

    /*should find dtype and the dtype of the fields*/
    title_Of_Info  = strtok(NULL,"\":' ,\n}{[]");
    dtype_Info   = strtok(NULL,"\":' ,\n}{[]");
    
    /*Should find length and the length of the field*/
    title_Of_Info = strtok(NULL,"\":' ,\n}{[]");
    length_Info  = strtok(NULL,"\":' ,\n}{[]");
    
    /*should find mtype and the mtype of the field*/
    title_Of_Info  = strtok(NULL,"\":' ,\n}{[]");
    mtype_Info   = strtok(NULL,"\":' ,\n}{[]" );
    
    fld = fld->next;


  }

  /*Unlock the memory*/
  AMS_Unlock_Memory(amem);


  return AMS_ERROR_NONE;
}

static int _P_cJSON_to_CStruct_Data(cJSON *specificField,AMS_Field fld)
{
  cJSON *jsonData;
  char  *boolString;
  int   j,k,data_Length;

  jsonData = cJSON_GetObjectItem(specificField,"data");
  data_Length = cJSON_GetArraySize(jsonData);

  for(j=0;j<data_Length;j++){

    
    /*only change data  if it is AMS_WRITE*/
    if(fld->mtype == AMS_WRITE){
      
      /*Switch on data type*/
      if(fld->dtype == AMS_STRING){
        free(((char**)fld->data)[j]);
        ((char**)fld->data)[j] = strdup((char*)cJSON_GetArrayItem(jsonData,j)->valuestring);
      }
      
      if(fld->dtype == AMS_CHAR){
        ((char*)fld->data)[j] = (char)cJSON_GetArrayItem(jsonData,j)->valuestring[0];
      }
      
      if(fld->dtype == AMS_DOUBLE){
        ((double*)fld->data)[j] = (double)cJSON_GetArrayItem(jsonData,j)->valuedouble;
      }        
      
      if(fld->dtype == AMS_FLOAT){
        ((float*)fld->data)[j] = (float)cJSON_GetArrayItem(jsonData,j)->valuedouble;
      }
      
      if(fld->dtype == AMS_INT){
        ((int*)fld->data)[j] = (int)cJSON_GetArrayItem(jsonData,j)->valueint;
      }
      
      if(fld->dtype == AMS_BOOLEAN){
        boolString = (char*)cJSON_GetArrayItem(jsonData,j)->valuestring;
        
        /*convert to lower case*/
        for(k=0;k<strlen(boolString);k++){
          boolString[k] = tolower(boolString[k]);
        }
        
        if(strcmp(boolString,"true") == 0) {
          ((int*)fld->data)[j]   = 1;
        } else if(strcmp(boolString,"false") == 0) {
          ((int*)fld->data)[j]   = 0;
        }
      }
      
    }
    
  }
  return AMS_ERROR_NONE;
  
}

static int _P_cJSON_to_CStruct_Field(cJSON *specificMemory,char *memoryName,char *fieldName)
{
  char       *currentName;
  cJSON      *fields,*specificField;
  int        i,j,k,nb_fields,data_Length,found_Memory_Flag = 0;
  int        found_Field_Flag = 0;
  AMS_Memory amem = memoryList;
  AMS_Field  fld;

  fields = cJSON_GetObjectItem(specificMemory,"field");
  nb_fields = cJSON_GetArraySize(fields);
    
  for(i=0;i<nb_fields;i++){
    
    specificField = cJSON_GetArrayItem(fields,i);
    currentName = cJSON_GetObjectItem(specificField,"name")->valuestring;

    /*find the memory and field in memoryList*/
    /*Find the named memory*/
    while(amem != NULL){
        
      if(strcmp(amem->name, memoryName) == 0){
        found_Memory_Flag = 1;
        break;
      }
      amem = amem->next;
    }
    if(found_Memory_Flag != 1) {
      return AMS_ERROR_MEMORY_NOT_FOUND;
    }

    fld = amem->fields;

    while(fld->next != NULL){

      if(strcmp(fld->name,currentName) == 0){
        found_Field_Flag = 1;
        break;
      }
      fld = fld->next;
    }
    if(found_Field_Flag != 1){
      return AMS_ERROR_FIELD_NOT_FOUND;
    }
    
    /*reset the found_Field_Flag*/
    found_Field_Flag = 0;


    /*if fieldName != NULL, only changed the specified field. IF fieldName is NULL, do all fields*/
    if(fieldName != NULL){
        
      if(strcmp(currentName,fieldName) != 0){
        
        _P_cJSON_to_CStruct_Data(specificField,fld);
      }
        
    } else {
      
      _P_cJSON_to_CStruct_Data(specificField,fld);

    }
    
    //printf("Field: %s\n",currentName);
    
  }

}

int cJSON_to_CStruct_Parser(char *JSON_Serialized_Object,char *memoryName,char *fieldName)
{
  cJSON *json,*memories,*specificMemory;
  int   i,j,nb_memories,nb_fields;
  char  *currentName;

  /*Parse the whole JSON*/
  json = cJSON_Parse(JSON_Serialized_Object);
  if (!json) {printf("Error before: [%s]\n",cJSON_GetErrorPtr());}

  if(!json){
  return AMS_ERROR_CJSON_ERROR;
  }

  /*Get just the memories and number of memories*/
 
  memories = cJSON_GetObjectItem(json,"memories");
  nb_memories = cJSON_GetArraySize(memories);
  


  /*loop through all memories, or find one specific*/
  for(i=0;i<nb_memories;i++){
    
    specificMemory = cJSON_GetArrayItem(memories,i);
    currentName = cJSON_GetObjectItem(specificMemory,"name")->valuestring;

    if(memoryName != NULL){
      
      if(strcmp(currentName,memoryName) != 0){
        
        _P_cJSON_to_CStruct_Field(specificMemory,memoryName,fieldName);
      }
    } else {

      _P_cJSON_to_CStruct_Field(specificMemory,currentName,fieldName);
    }
      
  }

  cJSON_Delete(json);
  return AMS_ERROR_NONE;



}
