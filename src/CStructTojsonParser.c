
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ams-private/amsprivate.h>

/* This simple function checks the AMS_Data_type and returns a string based on its value *
* None of the other petsc code returns anything but error codes, so maybe I shouldn't return strings */
static int dtype_Enum_to_String(AMS_Data_type dtype,char** dtype_String)
{
  if(dtype == AMS_DATA_UNDEF) {
    *dtype_String = "AMS_DATA_UNDEF";
    return AMS_ERROR_NONE;
  }

  if(dtype == AMS_CHAR) {
    *dtype_String =  "AMS_CHAR";
    return AMS_ERROR_NONE;
  }

  if(dtype == AMS_BOOLEAN) {
    *dtype_String = "AMS_BOOLEAN";
    return AMS_ERROR_NONE;
  }

  if(dtype == AMS_FLOAT) {
    *dtype_String = "AMS_FLOAT";
    return AMS_ERROR_NONE;
  }

  if(dtype == AMS_DOUBLE) {
    *dtype_String = "AMS_DOUBLE";
    return AMS_ERROR_NONE;
  }

  if(dtype == AMS_STRING) {
    *dtype_String = "AMS_STRING";
    return AMS_ERROR_NONE;
  }

  if(dtype == AMS_INT) {
    *dtype_String = "AMS_INT";
    return AMS_ERROR_NONE;

  }

  return AMS_ERROR_NONE;
}


/* This simple function checks the AMS_Memory_type and returns a string based on its value *
 * None of the other petsc code returns anything but error codes, so maybe I shouldn't return strings */
static int mtype_Enum_to_String(AMS_Memory_type mtype,char** mtype_String)
{
  if(mtype == AMS_MEMORY_UNDEF) {
    *mtype_String = "AMS_MEMORY_UNDEF";
    return AMS_ERROR_NONE;
  }

  if(mtype == AMS_READ) {
    *mtype_String = "AMS_READ";
    return AMS_ERROR_NONE;
  }

  if(mtype == AMS_WRITE) {
   *mtype_String = "AMS_WRITE";
   return AMS_ERROR_NONE;
  }

  return AMS_ERROR_NONE;
}

/* This function turns the data into a string, using AMS_Data_type to decide what case to use */
static int data_to_String(AMS_Data_type dtype,void *data,char *data_String,int length)

{

  int   i;
  char  *value_String = malloc(1024);
  sprintf(data_String,"\"data\": [");

  /*Loop over the length of the array, printing each to a field of the data string*/
  for(i=0;i<length;i++){
    /*? How should we handle this ?*/
    if(dtype == AMS_DATA_UNDEF) {
      //return "AMS_DATA_UNDEF";
    }

    if(dtype == AMS_CHAR) {
      sprintf(value_String,"\"%c\"",((char*)data)[i]);
      strcat(data_String, value_String);
    }
    
    if(dtype == AMS_BOOLEAN) {
      sprintf(value_String,"\"%s\"",((int*)data)[i] ? "true" : "false");
      strcat(data_String, value_String);
    }

    if(dtype == AMS_FLOAT) {
      sprintf(value_String,"%f",((float*)data)[i]);
      strcat(data_String, value_String);
    }

    if(dtype == AMS_DOUBLE) {
      sprintf(value_String,"%lf",((double*)data)[i]);
      strcat(data_String, value_String);
    }

    if(dtype == AMS_STRING) {
      sprintf(value_String,"\"%s\"",((char**)data)[i]);
      strcat(data_String, value_String);
    }

    if(dtype == AMS_INT) {
      sprintf(value_String,"%d",((int*)data)[i]);
      strcat(data_String, value_String);
    }

    /*Add a comma (for JSON format) on all but the last*/
    if((i+1) != length){
      strcat(data_String, ",");
    }

  }

  strcat(data_String, "]");

  free(value_String);
  return AMS_ERROR_NONE;
}

/* This function turns the data into a string, using AMS_Data_type to decide what case to use */
static int alternatives_to_String(AMS_Data_type dtype,void *data,char *data_String,int length)

{

  int   i;
  char  *value_String = malloc(1024);
  sprintf(data_String,"\"alternatives\": [");

  /*Loop over the length of the array, printing each to a field of the data string*/
  for(i=0;i<length;i++){
    /*? How should we handle this ?*/
    if(dtype == AMS_DATA_UNDEF) {
      //return "AMS_DATA_UNDEF";
    }

    if(dtype == AMS_CHAR) {
      sprintf(value_String,"\"%c\"",((char*)data)[i]);
      strcat(data_String, value_String);
    }
    
    if(dtype == AMS_BOOLEAN) {
      sprintf(value_String,"\"%s\"",((int*)data)[i] ? "true" : "false");
      strcat(data_String, value_String);
    }

    if(dtype == AMS_FLOAT) {
      sprintf(value_String,"%f",((float*)data)[i]);
      strcat(data_String, value_String);
    }

    if(dtype == AMS_DOUBLE) {
      sprintf(value_String,"%lf",((double*)data)[i]);
      strcat(data_String, value_String);
    }

    if(dtype == AMS_STRING) {
      sprintf(value_String,"\"%s\"",((char**)data)[i]);
      strcat(data_String, value_String);
    }

    if(dtype == AMS_INT) {
      sprintf(value_String,"%d",((int*)data)[i]);
      strcat(data_String, value_String);
    }

    /*Add a comma (for JSON format) on all but the last*/
    if((i+1) != length){
      strcat(data_String, ",");
    }

  }

  strcat(data_String, "]");

  free(value_String);
  return AMS_ERROR_NONE;
}


/* This function takes a memory and converts it, including all of its fields, into a JSON Serialized String */

int JSONParser_CStruct_to_JSON(char *final_JSON_Serialized_Object)
{

 
  AMS_Memory amem = memoryList;
  
  sprintf(final_JSON_Serialized_Object,"{\"memories\": [\n");


  while(amem != NULL){

    JSONParser_Single_CStruct_to_JSON(final_JSON_Serialized_Object,amem->name,1);
    amem = amem->next;
    if(amem != NULL) {
      strcat(final_JSON_Serialized_Object,",");
    }
  }

  /* write the final part of the string */
  strcat(final_JSON_Serialized_Object,"]\n");
  strcat(final_JSON_Serialized_Object, "}\n");

  return AMS_ERROR_NONE;

}



/* This function takes a memory and converts it, including all of its fields, into a JSON Serialized String 
 *if we are being called from the function to do all memories, allFlag should be set to 1*/

int JSONParser_Single_CStruct_to_JSON(char *final_JSON_Serialized_Object,char *name,int allFlag)
{

  int      nb_fields,i=0,j=0,found_Memory_Flag = 0;

  /* Declare variables to convert each individual object to a string (which will cocatenated) */
  /*! find a better way to dynamically allocate memory !*/
  char       *name_String;
  AMS_Field  fld;
  AMS_Memory amem = memoryList;
  name_String               = malloc(160);

  /*If we are called from the function to do all memories, don't sprintf memories; that is already done*/
  if(!allFlag){
    sprintf(final_JSON_Serialized_Object,"{\"memories\": [\n");
  }

  /*Find the named memory*/
  while(amem != NULL){

    if(strcmp(amem->name, name) == 0){
      found_Memory_Flag = 1;
      break;
    }
    amem = amem->next;
  }

  if(found_Memory_Flag != 1) {
    return AMS_ERROR_MEMORY_NOT_FOUND;
  }
    
  nb_fields = amem->nb_fields;

  sprintf(name_String,"{\"name\": \"%s\",\n",amem->name);

  strcat(final_JSON_Serialized_Object,name_String);
    /* write the first part of the string */
  strcat(final_JSON_Serialized_Object,"\n \"field\": [\n");

  fld = amem->fields;
    
  /* Loop over fields to obtain all the data from each name */
  for(i=0; i<nb_fields; i++) {
    
    JSONParser_Single_Field_CStruct_to_JSON(final_JSON_Serialized_Object,amem->name,fld->name,1);

    fld = fld->next;
    if(i + 1 != nb_fields){
      strcat(final_JSON_Serialized_Object,",");
    }
  }


  /* write the final part of the string */
  strcat(final_JSON_Serialized_Object,"]\n}");

  
  
  if(!allFlag){
    /* write the final part of the string */
    strcat(final_JSON_Serialized_Object,"]\n");
    strcat(final_JSON_Serialized_Object, "}\n");
  }
  //printf("%s", final_JSON_Serialized_Object);
  /* free the memory */

  free(name_String);



  return AMS_ERROR_NONE;

}

/* This function takes a specific field and converts it into a JSON Serialized String 
 * if all fields are to be grabbed, allFlag should be set to 1.*/

int JSONParser_Single_Field_CStruct_to_JSON(char *final_JSON_Serialized_Object,char *memoryName,char* fieldName,int allFlag)
{

  int      nb_fields,i=0,j=0, found_Memory_Flag;

  /* Declare variables to convert each individual object to a string (which will cocatenated) */
  /*! find a better way to dynamically allocate memory !*/
  char       *name_String,*data_String,*len_String = malloc(800),*dtype_String = malloc(800);
  char       *dtype, *mtype,*alternatives_String = malloc(3000);
  char       *mtype_String   = malloc(800);
  AMS_Field  fld;
  AMS_Memory amem = memoryList;
  data_String               = malloc(1060);
  name_String               = malloc(1060);

  /*only sprintf if we are not doing all fields; the higher function will take care of it*/
  if(!allFlag){
    sprintf(final_JSON_Serialized_Object,"{\"memories\": [\n");
  }

  /*Find the named memory*/
  while(amem != NULL){

    if(strcmp(amem->name, memoryName) == 0){
      found_Memory_Flag = 1;
      break;
    }
    amem = amem->next;
  }

  if(found_Memory_Flag != 1) {
    return AMS_ERROR_MEMORY_NOT_FOUND;
  }
    
  nb_fields = amem->nb_fields;

  char *JSON_Serialized_Object;

  if(!allFlag){
    sprintf(name_String,"{\"name\": \"%s\",\n",amem->name);
    strcat(final_JSON_Serialized_Object,name_String);
    /* write the first part of the string */
    strcat(final_JSON_Serialized_Object,"\n \"field\": [\n");
  }

  fld = amem->fields;
  
  /*Reset the found flag to use with the fields*/
  found_Memory_Flag = 0;
  /*Find the named field*/
  while(fld->next != NULL){

    if(strcmp(fld->name,fieldName) == 0){
      found_Memory_Flag = 1;
      break;
    }
    fld = fld->next;
  }

  
  if(found_Memory_Flag != 1) {
    return AMS_ERROR_FIELD_NOT_FOUND;
  }


  /* Allocate the right amount of memory */
  /*! find a better way to dynamically allocate memory !*/
  JSON_Serialized_Object = malloc(1024);
  
    /* Convert the individual field info into individual strings */
  dtype_Enum_to_String(fld->dtype,&dtype);
  sprintf(dtype_String,"\"dtype\": \"%s\"",dtype);
  mtype_Enum_to_String(fld->mtype,&mtype);
  sprintf(mtype_String,"\"mtype\": \"%s\"",mtype);
  sprintf(name_String,"\"name\": \"%s\"",fld->name);
  data_to_String(fld->dtype,fld->data,data_String,fld->len);
  sprintf(len_String,"\"length\": \"%d\"",fld->len);
  if(fld->alternatives != NULL){
    alternatives_to_String(fld->dtype,fld->alternatives,alternatives_String,fld->altLen);
  } else {
    sprintf(alternatives_String,"\"alternatives\": []");
  }
  
  /* turn the individual field into one long string */
  sprintf(JSON_Serialized_Object,"{\n %s,\n %s,\n %s, \n %s, \n %s, \n %s }",name_String,data_String,dtype_String,len_String,mtype_String,alternatives_String);
 
  


  
    
  /* write each field to the final string */
  strcat(final_JSON_Serialized_Object,JSON_Serialized_Object);
  strcat(final_JSON_Serialized_Object,"\n");
  
  if(!allFlag){
    /* write the final part of the string */
    strcat(final_JSON_Serialized_Object,"]\n}");
 


    /* write the final part of the string */
    strcat(final_JSON_Serialized_Object,"]\n");
    
    strcat(final_JSON_Serialized_Object, "}\n");
  }
  //printf("%s", final_JSON_Serialized_Object);
  /* free the memory */

  free(name_String);
  free(data_String);
  free(dtype_String);
  free(mtype_String);
  free(len_String);
  free(JSON_Serialized_Object);
  free(alternatives_String);



  return AMS_ERROR_NONE;

}
