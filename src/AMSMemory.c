#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ams-private/amsprivate.h>
#include <pthread.h>


/*I used an extern to allow the request handler to find the memory
 *and this is the global call that goes with it.
 *
 *I was taugh global calls are evil, so there may be a better way
 *but it was not apparent to me*/
AMS_Memory memoryList;


/*Adds a memory to memoryList. Only called when AMS_Memory_Create
 * is called
 */
static int _P_AMS_Memory_List_Add(AMS_Memory amem)
{
  AMS_Memory currentMemory = amem;

  /*the first memory*/
  if(memoryList == NULL){
    memoryList = currentMemory;
  } else {
    /*the rest of the memories*/
    currentMemory = amem;
    currentMemory->next = memoryList;
    memoryList = currentMemory;
  }
    
  return AMS_ERROR_NONE;
}

/*Removes a memory from memoryList. Only called when AMS_Memory_Destroy
 *is called.
 */
static int _P_AMS_Memory_List_Delete(AMS_Memory amem)
{
  AMS_Memory currentMemory, previousMemory;

  previousMemory = NULL;

  currentMemory = memoryList;

  while(currentMemory != NULL){

    /*If there pointers are the same, they are the same field
     *I use this instead of name, in case a user decides to name
     *two fields the same thing */
    if(currentMemory->fields == amem->fields){
      if(previousMemory == NULL){
        /*First of the list*/
        memoryList = currentMemory->next;
      } else {
        /* All other nodes */
        previousMemory->next = currentMemory->next;
      }
      return AMS_ERROR_NONE;
    }
    previousMemory = currentMemory;
    currentMemory  = currentMemory->next;
  }

  
  return AMS_ERROR_NONE;


}

/* Creats a nullfield for othe functions to use
 */
static int null_Field(AMS_Field *fld)
{
  (*fld)               = (AMS_Field) malloc(sizeof(struct _P_AMS_Field));
  /*add null fields*/
  (*fld)->name         = NULL;
  (*fld)->data         = NULL;
  (*fld)->alternatives = NULL;
  (*fld)->altLen       = 0;
  (*fld)->len          = -1;
  (*fld)->mtype        = AMS_MEMORY_UNDEF;
  (*fld)->dtype        = AMS_DATA_UNDEF;
  (*fld)->status       = 0;
  (*fld)->prev         = NULL;
  (*fld)->next         = NULL;
  return AMS_ERROR_NONE;
}


/*This function copies the data from one field to another
 * so that it is not copying the pointer address
 * fld1 is the destination, fld2 is the source 
 *Currently not being used for anything, but good to have.
 */
static int _P_AMS_Copy_Field(AMS_Field *fld,AMS_Field fld2,AMS_Field tailField)
{
  AMS_Field fld1      = (AMS_Field) malloc(sizeof(struct _P_AMS_Field));

  if(fld2->dtype == AMS_CHAR) {
    char *dataChar = malloc(sizeof(char));
    *dataChar      =  *(char*)fld2->data;
    fld1->data     = dataChar;
  }

  if(fld2->dtype == AMS_DOUBLE) {
    double *dataDouble = malloc(sizeof(double));
    *dataDouble        =  *(double*)fld2->data;
    fld1->data         = dataDouble;
  }

  if(fld2->dtype == AMS_FLOAT) {
    float *dataFloat = malloc(sizeof(float));
    *dataFloat       = *(float*)fld2->data;
    fld1->data       = dataFloat;
  }

  if(fld2->dtype == AMS_STRING) {
    fld1->data       = strdup((char*)fld2->data);
  }

  if(fld2->dtype == AMS_INT || fld2->dtype == AMS_BOOLEAN) {
    int *dataInt = malloc(sizeof(int));
    *dataInt     = *(int*)fld2->data;
    fld1->data   = dataInt;
  }

  fld1->name       = strdup(fld2->name);
  fld1->len        = fld2->len;
  fld1->mtype      = fld2->mtype;
  fld1->dtype      = fld2->dtype;
  fld1->status     = fld2->status;

  /*? Is this if statement sufficient to see if this is the first field? It works ?*/
  if(*fld == NULL) {
    fld1->prev      = NULL;
    fld1->next      = tailField;
    tailField->prev = fld1;
    //free(*fld);
    *fld            = fld1;
    /* The rest of the fields */ 
  } else {
    fld1->next       = tailField;
    fld1->prev       = tailField->prev;
    fld1->prev->next = fld1;
    tailField->prev  = fld1;
  }

  return AMS_ERROR_NONE;
}

/* This function frees an individual field */
static int _P_AMS_Free_Single_Field(AMS_Field fld)
{
  if(fld->name) free(fld->name);
  free(fld);

  return AMS_ERROR_NONE;

}

/* This function frees the whole list of fields */
static int _P_AMS_Free_Field_List(AMS_Field fld)
{
  AMS_Field individualFld = fld;

  /*? Maybe I shouldn't use a while loop ?*/
  while(individualFld->next != NULL) {
    individualFld = individualFld->next;
    _P_AMS_Free_Single_Field(individualFld->prev);
  }

  _P_AMS_Free_Single_Field(individualFld);

    return AMS_ERROR_NONE;
}


/*Adds a list of legal values to a field. Accepts the memory where the field is located,
 *the field name, the number of legal values, and a void* list of legal values */
int AMS_Field_Legal_Values(AMS_Memory amem,char* fldName,int numberOfValues,void *legalValues)
{
  AMS_Field fld = amem->fields;
  int       found_Field_Flag = 0;

  /*Find the named field*/
  while(fld->next != NULL){

    if(strcmp(fld->name,fldName) == 0){
      found_Field_Flag = 1;
      break;
    }
    fld = fld->next;
  }

  if(found_Field_Flag != 1){
    return AMS_ERROR_FIELD_NOT_FOUND;
  }

  fld->altLen       = numberOfValues;
  fld->alternatives = legalValues;

  return AMS_ERROR_NONE;



}


/* Adds a field to the declared memory with the given parameters
 * AMS_Memory amem - the memory the field is to be added to
 * char *name - a string with the desired name of the added fields
 * void *data - the data to be added to the field
 * int len - the length of that data array
 * AMS_Memory_type mtype - the memory type (read/write)
 * AMS_Data_type dtype - the data type (int, char, bool, etc)
 */
int AMS_New_Field(AMS_Memory amem,const char *name,void *data,int len,AMS_Memory_type mtype,AMS_Data_type dtype)
{

 
  AMS_Field fld   = (AMS_Field) malloc(sizeof(struct _P_AMS_Field));

  fld->name         = strdup(name);
  fld->data         = data;
  fld->alternatives = NULL;
  fld->altLen       = 0;
  fld->len          = len;
  fld->mtype        = mtype;
  fld->dtype        = dtype;
  fld->status       = 0;
  
  /*Lock the memory before anyfield is added*/
  AMS_Lock_Memory(amem);

  /* The first field */
  if(amem->fields == amem->tailField) {
    fld->prev             = NULL;
    fld->next             = amem->tailField;
    amem->fields          = fld;
    amem->tailField->prev = fld;
    /* The rest of the fields */ 
  } else {
    fld->next             = amem->tailField;
    fld->prev             = amem->tailField->prev;
    fld->prev->next       = fld;
    amem->tailField->prev = fld;
  }
  amem->nb_fields = amem->nb_fields + 1;
  
  /*Unlock the memory*/
  AMS_Unlock_Memory(amem);
  return AMS_ERROR_NONE;
}

/*Creates a memory. Accepts an argument of name and a memory.
 *To be useful, fields must be added to the memory
 */
int AMS_Memory_Create(const char *name, AMS_Memory *amem)
{
  (*amem) = (AMS_Memory) malloc(sizeof(struct _P_AMS_Memory));
  AMS_Field fld;
  null_Field(&fld);
  /*? I am strduping a non malloc'd string. Is that a problem?*/
  (*amem)->name        = strdup(name);
  (*amem)->nb_fields   = 0;
  (*amem)->fields      = fld;
  (*amem)->tailField   = fld;
  (*amem)->next        = NULL;
  (*amem)->nb_memories = 0;
  /*initialize the mutex */
  pthread_mutex_init(&((*amem)->mem_Lock), NULL);

  
 
  _P_AMS_Memory_List_Add(*amem);
  return AMS_ERROR_NONE;
}

/*Frees the memory, including all of the memories fields and all of the fields
 *data
 */
int AMS_Memory_Destroy(AMS_Memory *amem)
{
  /*Lock the memory*/
  AMS_Lock_Memory(*amem);
  _P_AMS_Memory_List_Delete(*amem);
  _P_AMS_Free_Field_List((*amem)->fields);

  /*unlock the memory, destroy the mutex*/
  AMS_Unlock_Memory(*amem);
  pthread_mutex_destroy(&((*amem)->mem_Lock));
  free((*amem)->name);
  free(*amem);

  *amem = NULL;

  return AMS_ERROR_NONE;
}


/*Using a simple mutex, this function locks the memory.
 *should be called anytime a memory's data is being changed
 */
int AMS_Lock_Memory(AMS_Memory amem)
{
  pthread_mutex_lock(&(amem->mem_Lock));
  return AMS_ERROR_NONE;
}

/*Unlocks a simple mutex. Should be called after AMS_Lock_Memory,
 *once the calling thread has finished changing the data
 */
int AMS_Unlock_Memory(AMS_Memory amem)
{
  pthread_mutex_unlock(&(amem->mem_Lock));
  return AMS_ERROR_NONE;

}

