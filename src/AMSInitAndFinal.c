
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ams-private/amsprivate.h>
#include <ams-private/mongoose.h>

/* The header for the http call */
static const char *ajax_reply_start =
  "HTTP/1.1 200 OK\r\n"
  "Cache: no-cache\r\n"
  "Content-Type: application/javascript\r\n"
  "\r\n";

 /* Global static call so that it can be used by AMS_Initialize and AMS_Finalize
  * bot hwithout needing to be passed. This is probably bad form, but I did not
  * see a better option. */
static struct mg_context *ctx;

static int _P_AMS_JSON_Memory_Name_List(char *final_JSON)
{
  AMS_Memory amem;

  sprintf(final_JSON, "{\"names\":[");
  amem = memoryList; 

  while(amem != NULL) {
    
    strcat(final_JSON, "\"");
    strcat(final_JSON, amem->name);
    strcat(final_JSON, "\"");

    amem = amem->next;
    
    /*Add a comma for JSON purposes*/

    if(amem != NULL) {
      strcat(final_JSON, ",");
    }
  
  }

  /*Add the final JSON parts*/
  strcat(final_JSON, "]}");
  
  return AMS_ERROR_NONE;
}

/* This handler gets all of the memory names and posts them to screen */
static int ajax_get_memory_names(struct mg_connection *conn,const struct mg_request_info *request_info)
{
  char *final_JSON = malloc(102400);

  /*get the names from the memories*/
  _P_AMS_JSON_Memory_Name_List(final_JSON);

  // Prepare the message we're going to send
  int content_length = strlen(final_JSON);
  mg_printf(conn, "%s", ajax_reply_start);
  mg_printf(conn, "%s", final_JSON);

  free(final_JSON);
   
  return AMS_ERROR_NONE;
}

static int ajax_get_single_memory(struct mg_connection *conn,const struct mg_request_info *request_info, char *name)
{
  char *final_JSON = malloc(4096);
  int  errorCode;
  int  data_len;
  char *inputDataPointer;

  
  /* parse the memory */
  errorCode = JSONParser_Single_CStruct_to_JSON(final_JSON,name,0);

  if(errorCode == AMS_ERROR_MEMORY_NOT_FOUND) {
    mg_printf(conn,"%s",ajax_reply_start);
    mg_printf(conn,"memory not found");
  } else {

    // Prepare the message we're going to send
    int content_length = strlen(final_JSON);
    mg_printf(conn, "%s", ajax_reply_start);
    mg_printf(conn, "%s", final_JSON);
  }
  free(final_JSON);
  
  return AMS_ERROR_NONE;
}

static int ajax_get_single_field(struct mg_connection *conn,const struct mg_request_info *request_info, char *memoryName,char *fieldName)
{
  char *final_JSON = malloc(4096);
  int  errorCode;
  int  data_len;
  char *inputDataPointer;

  
  /* parse the memory */
  errorCode = JSONParser_Single_Field_CStruct_to_JSON(final_JSON,memoryName,fieldName,0);

  if(errorCode == AMS_ERROR_MEMORY_NOT_FOUND) {
    mg_printf(conn,"%s",ajax_reply_start);
    mg_printf(conn,"memory not found");
  } else if(errorCode == AMS_ERROR_FIELD_NOT_FOUND) {
    mg_printf(conn,"%s",ajax_reply_start);
    mg_printf(conn,"field not found");
  } else {
    // Prepare the message we're going to send
    int content_length = strlen(final_JSON);
    mg_printf(conn, "%s", ajax_reply_start);
    mg_printf(conn, "%s", final_JSON);
  }
  free(final_JSON);
  
  return AMS_ERROR_NONE;
}


/* This handler posts  the data to screen, and stores the memory on the website */
static int ajax_get_all_memories(struct mg_connection *conn,const struct mg_request_info *request_info)
{
 
  char *final_JSON = malloc(409600);

  /* parse the field */
  JSONParser_CStruct_to_JSON(final_JSON);


  // Prepare the message we're going to send
  int content_length = strlen(final_JSON);
  mg_printf(conn, "%s", ajax_reply_start);
  mg_printf(conn, "%s", final_JSON);
  printf("All memories\n");

  free(final_JSON);
   
  return AMS_ERROR_NONE;
}

/* This handler alters the field data from the input given in the JS page */
static int ajax_Change_Memory(struct mg_connection *conn,const struct mg_request_info *request_info,char *name)
{

  int  sizeOfDataArray = 2048;
  char *totalData      = malloc(sizeOfDataArray),*inputData = malloc(sizeOfDataArray);
  int  data_len;
  char *inputDataPointer;

  /*mg_read reads all the data from the request*/
  mg_read(conn, totalData, sizeOfDataArray);

  /*mg_get_var searches the data received for only the "input" field */
  mg_get_var(totalData, sizeOfDataArray, "input", inputData, sizeOfDataArray);

  inputDataPointer = strdup(inputData);

  printf("Data: %s\n", inputDataPointer);
  cJSON_to_CStruct_Parser(inputDataPointer,name,NULL);
  free(inputDataPointer);
  free(totalData);
  free(inputData);

  return AMS_ERROR_NONE;
 
}

/* This handler alters the field data from the input given in the JS page */
static int ajax_Change_All_Memories(struct mg_connection *conn,const struct mg_request_info *request_info)
{

  int  sizeOfDataArray = 4048;
  char *totalData      = malloc(sizeOfDataArray),*inputData = malloc(sizeOfDataArray);
  int  data_len;
  char *inputDataPointer;

  /*mg_read reads all the data from the request*/
  mg_read(conn, totalData, sizeOfDataArray);

  /*mg_get_var searches the data received for only the "input" field */
  mg_get_var(totalData, sizeOfDataArray, "input", inputData, sizeOfDataArray);

  inputDataPointer = strdup(inputData);

  printf("Data: %s\n", inputDataPointer);
  cJSON_to_CStruct_Parser(inputDataPointer,NULL,NULL);
  free(inputDataPointer);
  free(totalData);
  free(inputData);

  return AMS_ERROR_NONE;
 
}

/*this is the main handler call. It switched based on what uri is in the request*/
static int begin_request_handler(struct mg_connection *conn)
{
  const struct mg_request_info *request_info = mg_get_request_info(conn);
  int  processed = 1;
  char *stringChunk,*memoryName,*fieldName; 
  char *requestInfo = strdup(request_info->uri);


  /*This handler uses strtok to break it up by where the / is (like a directory)
   *And goes to the appropriate place based on the information from strtok
   */
  stringChunk = strtok(requestInfo, "/");
  
  if(stringChunk == NULL) {
    mg_send_file(conn,"newPage.html");
  } else if(strcmp(stringChunk,"ams") == 0) {
    stringChunk = strtok(NULL, "/");
    if(stringChunk == NULL) {
    } else if(strcmp(stringChunk,"memory") == 0) {
      /* /ams/memory */
      stringChunk = strtok(NULL, "/");
      if(stringChunk == NULL) {
        ajax_get_memory_names(conn,request_info);
        printf("memory names \n");
      } else if(strcmp(stringChunk,"*") == 0) {
        /* /ams/memory/* */
        if(strcmp(request_info->request_method, "GET") == 0) {
            /*GET*/
            ajax_get_all_memories(conn,request_info);
          } else if(strcmp(request_info->request_method, "POST") == 0) {
            /*POST*/
            ajax_Change_All_Memories(conn,request_info);
          }
      } else {
        /* /ams/memory/memname */
        memoryName = stringChunk;
        stringChunk = strtok(NULL, "/");
        if(stringChunk == NULL) {
          if(strcmp(request_info->request_method, "GET") == 0) {
            /*GET*/
            ajax_get_single_memory(conn,request_info,memoryName);
          } else if(strcmp(request_info->request_method, "POST") == 0) {
            /*POST*/
            ajax_Change_Memory(conn,request_info,memoryName);
          }
          printf("name: %s \n",memoryName);
        } else if(strcmp(stringChunk,"field") == 0){
          /* /ams/memory/memname/field */
          stringChunk = strtok(NULL, "/");
          
          if(stringChunk == NULL){
            /* just /ams/memory/memname/field */
          } else {
            /* /ams/memory/memname/field/fieldname */
            fieldName = stringChunk;
            ajax_get_single_field(conn,request_info,memoryName,fieldName);
            printf("memory: %s field %s \n", memoryName,fieldName);
          }
        }
      }
    } 
  } else {
    processed = 0;
    printf("Did not process\n");
  }

  free(requestInfo);
  return processed;

}


/*Initializes the server. We can accept command line arguments through here, I
 *presume, to affect things like listening ports for mongoose*/
int AMS_Initialize()
{
  /* List of options. Last element must be NULL. */
  const char *options[] = {"listening_ports", "8080", "num_threads", "1", "document_root", "./",NULL};
  struct mg_callbacks callbacks;

  /* Prepare callbacks structure. We have only one callback, the rest are NULL. */
  memset(&callbacks, 0, sizeof(callbacks));
  callbacks.begin_request = begin_request_handler;

  /*Start the web server.*/
  ctx = mg_start(&callbacks, NULL, options);

  return AMS_ERROR_NONE;

}

/*stop the server with the simple mg_stop() call. Maybe we will clean up threads here
 *later, as well.*/
int AMS_Finalize()
{
  mg_stop(ctx);
  return AMS_ERROR_NONE;
}
