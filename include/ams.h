#if !defined(__ams_h__)
#define __ams_h__

typedef enum {AMS_MEMORY_UNDEF,AMS_READ,AMS_WRITE} AMS_Memory_type;
typedef enum {AMS_DATA_UNDEF,AMS_CHAR,AMS_BOOLEAN,AMS_INT,AMS_FLOAT,AMS_DOUBLE,AMS_STRING} AMS_Data_type;

typedef struct _P_AMS_Memory* AMS_Memory;

/*Memory and field creation */
int AMS_New_Field(AMS_Memory,const char*,void*,int,AMS_Memory_type,AMS_Data_type);
int AMS_Field_Legal_Values(AMS_Memory,char*,int,void*);
int AMS_Memory_Create(const char*,AMS_Memory*);
int AMS_Memory_Destroy(AMS_Memory*);
int AMS_Initialize();
int AMS_Finalize();
int AMS_Lock_Memory(AMS_Memory);
int AMS_Unlock_Memory(AMS_Memory);
#endif
