#if !defined(__amsprivate_h__)
#define __amsprivate_h__

#include "ams.h"
#include "pthread.h"

/*
    Each field represents a multi-dimensional array indexed from start[l] to end[l]-1 for each l < dim
    len = (end[0] - 1 - start[0])*(end[1] - 1 - start[1]) ... (end[dim-1] - 1 - start[dim-1])
*/

typedef struct _P_AMS_Field* AMS_Field;

struct _P_AMS_Field
{
  char            *name;
  void            *data;     /* raw data  */
  int             len;
  int             *start;
  int             *end;
  AMS_Memory_type mtype;
  AMS_Data_type   dtype;
  int             status;           /* 0 = unmodified, 1 = modified */
  AMS_Field       prev;             /* pointer to previous added field */
  AMS_Field       next;             /* pointer to next added field */
  void            *alternatives;
  int             altLen;
};

struct _P_AMS_Memory
 {
   char            *name;
   AMS_Field       fields;
   AMS_Field       tailField;
   int             nb_fields;
   AMS_Memory      next;
   int             nb_memories;
   pthread_mutex_t mem_Lock;
};

 /*cstruct to JSON parser*/
int JSONParser_CStruct_to_JSON(char*);
int JSONParser_Single_CStruct_to_JSON(char*,char*,int);


/*json to cStruct parser*/
int JSONParser_JSON_to_CStruct(char*);
int JSONParser_Single_JSON_to_CStruct(char*,char*,int);
int JSONParser_Single_Field_JSON_to_CStruct(char*,char*,char*,int);
int cJSON_to_CStruct_Parser(char*,char*,char*);



/*I used an extern to allow the request handler to find the memory.
 *I was taugh global calls are evil, so there may be a better way
 *but it was not apparent to me
 *This is only in AMSInitAndFinal.c and AMSMemory. If this is changed, lines (as of the writing
 *of this comment) 65 and 98 of AMSInitAndFinal.c */
extern AMS_Memory memoryList;

#define AMS_ERROR_NONE               0
#define AMS_ERROR_MEMORY_NOT_FOUND   1
#define AMS_ERROR_FIELD_NOT_FOUND    2
#define AMS_ERROR_NOT_NUMERIC        3
#define AMS_ERROR_NOT_BOOLEAN        4
#define AMS_ERROR_DATATYPE_NOT_FOUND 5
#define AMS_ERROR_CJSON_ERROR        6
#endif
