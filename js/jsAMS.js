

/*ams are the functions for sending back and forth; more private */
var ams = {
    // Backend URL, string.
    // 'http://backend.address.com' or '' if backend is the same as frontend
    backendUrl: '',
};

/*memories stores the data as memories['name'].field['name']*/
var memories = {}

/*domFunctions are all the functions that interact with the webpage; more public*/
var domFunctions = {};


ams.memoriesToAmsMemory = function(){

    for(var i = 0,len = ams.memory.memories.length;i<len;i++){
        for(var j = 0,len2 = ams.memory.memories[i].field.length;j<len2;j++){
            for(var k = 0,len3 = ams.memory.memories[i].field[j].data.length;k<len3;k++){
                ams.memory.memories[i].field[j].data = memories[ams.memory.memories[i].name].fields[ams.memory.memories[i].field[j].name].data
            }
        }
    }
    

}

/*This function tells the webserver to give it the list of names */
ams.getNames = function() {
   
    $.getJSON(ams.backendUrl + '/ams/memory', function(data){
        ams.names = data
    })
};


/*This function tells the webserver to give it a specific memory*/
ams.getMemory = function(names,update) {

    var i = 0
    if(!update){
        memories = {}
    }
    if(names == null){
        $.getJSON(ams.backendUrl + '/ams/memory/*', function(data){
            
            ams.memory = data
            
            for(var i = 0, len = data.memories.length; i<len;i++){
                
                if(!update){
                    memories[data.memories[i].name] = $.extend(true, {}, data.memories[i])
                    memories[data.memories[i].name].fields = {}
                }                

                for(var j = 0, len2 = data.memories[i].field.length; j < len2; j++){
                    memories[data.memories[i].name].fields[data.memories[i].field[j].name] = data.memories[i].field[j]
                }

            }
            
        })
    } else {
        $.each(names,function(key,value){
            if(names[key].length == 0){
                
                $.getJSON(ams.backendUrl + '/ams/memory/' + key, function(data){
                    ams.memory = data
                    for(var i = 0;i<data.memories.length;i++){
                        if(!update){
                            memories[data.memories[i].name] = $.extend(true, {}, data.memories[i])
                            memories[data.memories[i].name].fields = {}
                        }



                        for(var j = 0, len2 = data.memories[i].field.length; j < len2; j++){
                            memories[data.memories[i].name].fields[data.memories[i].field[j].name] = data.memories[i].field[j]
                            
                        }
                    }
                })
            } else {
                    
                for(var j = 0;j<names[key].length;j++){
                    $.getJSON(ams.backendUrl + '/ams/memory/' + key + '/field/' + names[key][j], function(data){
                        ams.memory = data
                        for(var i = 0;i<data.memories.length;i++){
                            if(!update){
                                if(!memories[data.memories[i].name]){

                                    memories[data.memories[i].name] = $.extend(true, {}, data.memories[i])
                                    memories[data.memories[i].name].fields = {}
                                }
                            }
                        

                            for(var l = 0;l<data.memories[i].field.length;l++){
                                memories[data.memories[i].name].fields[data.memories[i].field[l].name] = data.memories[i].field[l]
                            }
                        }
                    })
                             
                }
                
            }
           
        })
    }
};




/*Sends a POST of the JSON to the server*/
ams.postMemories = function(memory){
    var stringJSON = JSON.stringify(memory)
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: ams.backendUrl + '/ams/memory/*',
        data: {input: stringJSON}
    })
}



/*Stores the list of memory names and writes them to the screen as buttons*/
domFunctions.memoryNames = function() {
    
    //ams.memoriesToAmsMemory()
    /*Get the names from t he server*/
    ams.getNames()
    $("#memoryNames").html("")
    
    setTimeout(function(){
        for(var i = 0; i<ams.names.names.length; i++){
            $("#memoryNames").append("<input type=\"button\" class=\"memoryBtn\" value=\"" + ams.names.names[i] + "\" id=\"memory" + i + "\">")
            $("#memory" + i).data("number", i)
        }
    },125)
}

domFunctions.writeMemory = function(names){

    //ams.memoriesToAmsMemory()

    ams.getMemory(names,false)
    $("#fieldsInfo").html("")
    var k = 0;

    setTimeout(function(){           
        domFunctions.htmlWrite();
    },100)
}

domFunctions.htmlWrite = function(){

    var k = 0;
    var i = 0;
    var j = 0;

    

    $.each(memories, function(key, value) {
        
    
        $("#fieldsInfo").append("<h3>"+ key +":</h3>")
            
        i = 0

        $.each(memories[key].fields, function(fieldKey, fieldValue) {
        
            $("#fieldsInfo").append("<br><br><b>" + i + ": </b>Name: " + memories[key].fields[fieldKey].name)
            $("#fieldsInfo").append("<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data: ")
        
            for(j=0;j<memories[key].fields[fieldKey].data.length;j++){
                
                /*Only allow an input box if it is AMS_WRITE*/
                if(memories[key].fields[fieldKey].alternatives.length == 0){
                    if(memories[key].fields[fieldKey].dtype == "AMS_BOOLEAN") {
                        $("#fieldsInfo").append("<select id=\"data" + k + i + j + "\">")
                        $("#data" + k + i + j).append("<option value=\"true\">True</option> <option value=\"false\">False</option>")
                    } else {
	                $("#fieldsInfo").append("<input size=\"1\" type=\"data\" id=\"data" + k + i + j + "\" name=\"data\" \\>")
                    }
                    $("#data" + k + i + j).val(memories[key].fields[fieldKey].data[j])
                } else {
                    $("#fieldsInfo").append("<select id=\"data" + k + i + j + "\">")
                    $("#data" + k + i + j).append("<option value=\"" + memories[key].fields[fieldKey].data[j] + "\">" + memories[key].fields[fieldKey].data[j] + "</option>")
                    for(var l=0;l<memories[key].fields[fieldKey].alternatives.length;l++){
                        $("#data" + k + i + j).append("<option value=\"" + memories[key].fields[fieldKey].alternatives[l] + "\">" + memories[key].fields[fieldKey].alternatives[l] + "</option>")
                }
                    $("#fieldsInfo").append("</select>")
                }
                if(memories[key].fields[fieldKey].mtype != "AMS_WRITE") {
                    $("#data"+ k + i + j).attr('readonly',true)
                }
                
                
            }
            $("#fieldsInfo").append("<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dtype: " + memories[key].fields[fieldKey].dtype +
			            "<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mtype: " + memories[key].fields[fieldKey].mtype)
            i++
        })
        
        
        k++;
        
    })
}



domFunctions.refreshData = function(names){

 /*Get the data input from the text boxes */
   
    ams.getMemory(names,true)

    var k = 0;
    var i = 0;
    var j = 0;

    setTimeout(function(){
        if(names == null){
            
            $.each(memories, function(key, value) {            
                i = 0
                $.each(memories[key].fields, function(fieldKey, fieldValue) {
                    
                    for(var k = 0; k<memories[key].fields[fieldKey].data.length; k++){
                        
                        $("#data" + j + i + k).val(memories[key].fields[fieldKey].data[k])
                    }
                    i++
                })
                    
                j++
            })
                
                } else {
                    
                    $.each(names,function(namesKey,namesValue){
                        j=0
                        $.each(memories, function(key, value) {            
                            i = 0
                            $.each(memories[key].fields, function(fieldKey, fieldValue) {
                                
                                for(var k = 0; k<memories[key].fields[fieldKey].data.length; k++){
                                    if (namesKey == memories[key].name){
                                            for(var l = 0;l<names[namesKey].length;l++){
                                                if(names[namesKey][l] == memories[key].fields[fieldKey].name){
                                                    $("#data" + j + i + k).val(memories[key].fields[fieldKey].data[k])
                                                }
                                            }
                                    }
                                    
                                        
                                }
                                i++
                                
                            })
                            j++
                         })
                                
                        
                    })
                }
    },100)
}
              
    
/*postMemory sends the data from the browser to the C program*/
domFunctions.postAllMemories = function() {
    
    ams.memoriesToAmsMemory()

    /*Get the data input from the text boxes */
    for(var j = 0; j<ams.memory.memories.length; j++) {
        for(var i = 0; i<ams.memory.memories[j].field.length; i++){
            for(var k = 0; k<ams.memory.memories[j].field[i].data.length; k++){
                if($("#data" + j + i + k).length != 0) {
                    var data1 = $("#data" + j + i + k).val();
                    
                    if(ams.memory.memories[j].field[i].dtype == "AMS_INT"){     
                        
                        if(!isNaN(parseInt(data1))){
                            ams.memory.memories[j].field[i].data[k] = parseInt(data1)
                        }                   
                    } else if(ams.memory.memories[j].field[i].dtype == "AMS_DOUBLE" || ams.memory.memories[j].field[i].dtype == "AMS_FLOAT"){     
                        
                        if(!isNaN(parseFloat(data1))){
                            ams.memory.memories[j].field[i].data[k] = parseFloat(data1)
                        }
                        
                    } else {
                        ams.memory.memories[j].field[i].data[k] = data1
                    }
                    
                    
                    
                    
                }
            }
        }
    }

    ams.postMemories(ams.memory)
    
};

