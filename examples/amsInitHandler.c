
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ams.h"

int main(int argc,char **argv)
{
  AMS_Memory amem;
  AMS_Memory amem2;
    
  int         i,length             = 3;
  char        **stringData         = malloc(sizeof(char*));
  char        **stringDataCopy     = malloc(sizeof(char*));
  char        **stringDataArray    = malloc(length * sizeof(char*));
  char        **stringDataArrayCopy= malloc(length * sizeof(char*));
  int         *intData             = malloc(sizeof(int));
  int         *intDataArray        = malloc(length * sizeof(int)); 
  int         *intDataCopy         = malloc(sizeof(int));
  int         *intDataArrayCopy    = malloc(length * sizeof(int));
  double      *doubleData          = malloc(sizeof(double));
  double      *doubleDataArray     = malloc(length * sizeof(double));
  double      *doubleDataCopy      = malloc(sizeof(double));
  double      *doubleDataArrayCopy = malloc(length * sizeof(double));
  float       *floatData           = malloc(sizeof(float));
  float       *floatDataArray      = malloc(length * sizeof(float));
  float       *floatDataCopy       = malloc(sizeof(float));
  float       *floatDataArrayCopy  = malloc(length * sizeof(float));
  char        *charData            = malloc(sizeof(char));
  char        *charDataArray       = malloc(length * sizeof(char));
  char        *charDataCopy        = malloc(sizeof(char));
  char        *charDataArrayCopy   = malloc(length * sizeof(char));
  int         *boolData            = malloc(sizeof(int));
  int         *boolDataArray       = malloc(length * sizeof(int));
  int         *boolDataCopy        = malloc(sizeof(int));
  int         *boolDataArrayCopy   = malloc(length * sizeof(int));
  

  *boolData       = 1;
  *boolDataCopy   = 1;
  *intData        = 41;
  *intDataCopy    = 41;
  *doubleData     = 1.234567;
  *doubleDataCopy = 1.234567;
  *floatData      = 7.65432;
  *floatDataCopy  = 7.65432;
  *charData       = 'b';
  *charDataCopy   = 'b';
  *stringData     = strdup("string");
  *stringDataCopy = "string";

  for(i=0;i<length;i++){
    boolDataArray[i]       = 1;
    boolDataArrayCopy[i]   = 1;
    intDataArray[i]        = i + 12;
    intDataArrayCopy[i]    = i + 12;
    doubleDataArray[i]     = 27.7 + i;
    doubleDataArrayCopy[i] = doubleDataArray[i];
    floatDataArray[i]      = 13.3 + i;
    floatDataArrayCopy[i]  = 13.3 + i;
    charDataArray[i]       = 'b';
    charDataArrayCopy[i]   = 'b';
    stringDataArray[i]     = strdup("string");
    stringDataArrayCopy[i] = "string";
  }



  AMS_Initialize();

  /* construct a memory */
  AMS_Memory_Create("example2", &amem2);
  AMS_Memory_Create("example", &amem);

 /* add a field */
  AMS_New_Field(amem,"residual",boolData,1,AMS_WRITE,AMS_BOOLEAN);
  AMS_New_Field(amem2,"residual2",stringData,1,AMS_WRITE,AMS_STRING);
  AMS_New_Field(amem,"residual3",doubleData,1,AMS_WRITE,AMS_DOUBLE);
  AMS_New_Field(amem2,"residual4",intData,1,AMS_WRITE,AMS_INT);
  AMS_New_Field(amem,"residual5",floatData,1,AMS_WRITE,AMS_FLOAT);
  AMS_New_Field(amem2,"residual6",charData,1,AMS_WRITE,AMS_CHAR);
  AMS_New_Field(amem,"residual7",boolDataArray,length,AMS_WRITE,AMS_BOOLEAN);
  AMS_New_Field(amem2,"residual8",stringDataArray,length,AMS_WRITE,AMS_STRING);
  AMS_New_Field(amem,"residual9",doubleDataArray,length,AMS_WRITE,AMS_DOUBLE);
  AMS_New_Field(amem2,"residual10",intDataArray,length,AMS_WRITE,AMS_INT);
  AMS_New_Field(amem,"residual11",floatDataArray,length,AMS_WRITE,AMS_FLOAT);
  AMS_New_Field(amem2,"residual12",charDataArray,length,AMS_WRITE,AMS_CHAR);


  /*Change to while to see the updating information or to getchar() to allow a finishing run */

  //while(1){
  //(*intData)++;
  //}

  getchar();

  AMS_Memory_Destroy(&amem2);
  AMS_Memory_Destroy(&amem);

  AMS_Finalize();

  /*free all the user data*/
  free(intData);
  free(intDataArray);
  free(intDataCopy);
  free(intDataArrayCopy);
  free(*stringData);
  free(stringData);
  free(stringDataCopy);
  free(stringDataArrayCopy);
  for(i=0;i<length;i++){
    free(stringDataArray[i]);
  }
  free(stringDataArray);
  free(doubleData);
  free(doubleDataArray);
  free(doubleDataArrayCopy);
  free(doubleDataCopy);
  free(floatData);
  free(floatDataArray);
  free(floatDataCopy);
  free(floatDataArrayCopy);
  free(charData);
  free(charDataArray);
  free(charDataCopy);
  free(charDataArrayCopy);
  free(boolData);
  free(boolDataArray);
  free(boolDataCopy);
  free(boolDataArrayCopy);

}
