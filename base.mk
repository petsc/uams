# directory containing this file
BASEDIR := $(abspath $(dir $(lastword $(MAKEFILE_LIST))))

BINDIR := $(build_dir)/bin
LIBDIR := $(build_dir)/lib
AMS_SRC.c := $(shell cd $(BASEDIR) && find src -name \*.c)
AMS_SRC.o := $(AMS_SRC.c:%.c=$(build_dir)/%.o)
EXE_SRC.c := $(shell cd $(BASEDIR) && find examples tests -name \*.c)
EXE_SRC.o := $(EXE_SRC.c:%.c=$(build_dir)/%.o)
ALL_SRC.o := $(AMS_SRC.o) $(EXE_SRC.o)

vpath %.c $(BASEDIR)

libams := $(LIBDIR)/$(if $(SHARED),libams.so,libams.a)

CFLAGS += -I$(BASEDIR)/include
ifneq ($(SHARED),)
LDFLAGS_RPATH := -Wl,-rpath,$(dir $(libams))
endif

.SECONDEXPANSION:		# to expand $$(@D)/.DIR

$(build_dir)/%.o: %.c | $$(@D)/.DIR
	$(CC) -c -o $@ $< $(CFLAGS) $(if $(SHARED),$(CFLAGS_SHARED))

all: lib amsInitHandler simpleJSONtoCStructTest amsInitHandlerUpdatingData

simpleJSONtoCStructTest : $(BINDIR)/simpleJSONtoCStructTest
$(BINDIR)/simpleJSONtoCStructTest: $(build_dir)/tests/simpleJSONtoCStructTest.o $(libams) | $$(@D)/.DIR
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS) $(LDFLAGS_RPATH) $(LIBS)

amsInitHandler : $(BINDIR)/amsInitHandler
$(BINDIR)/amsInitHandler: $(build_dir)/examples/amsInitHandler.o $(libams) | $$(@D)/.DIR
	$(CC) -o $@ $(CFLAGS) $^ $(LDFLAGS) $(LDFLAGS_RPATH) $(LIBS)

amsInitHandlerUpdatingData : $(BINDIR)/amsInitHandlerUpdatingData
$(BINDIR)/amsInitHandlerUpdatingData: $(build_dir)/examples/amsInitHandlerUpdatingData.o $(libams) | $$(@D)/.DIR
	$(CC) -o $@ $(CFLAGS) $^ $(LDFLAGS) $(LDFLAGS_RPATH) $(LIBS)

lib: $(libams) $(build_dir)/include/ams.h
$(LIBDIR)/libams.a: $(AMS_SRC.o) | $$(@D)/.DIR
	ar rc $@ $^
$(LIBDIR)/libams.so: $(AMS_SRC.o) | $$(@D)/.DIR
	$(CC) -shared -o $@ $^ $(LDFLAGS) $(LIBS)

$(build_dir)/include/%.h : $(BASEDIR)/include/%.h | $$(@D)/.DIR
	cp $^ $@

install : lib
	install -d -m0755 "$(DESTDIR)$(libdir)"
	install -m0644 $(LIBDIR)/lib* "$(DESTDIR)$(libdir)"
	install -d -m0755 "$(DESTDIR)$(includedir)"
	install -m0644 $(BASEDIR)/include/*.h "$(DESTDIR)$(includedir)"

%/.DIR :
	@mkdir -p $(@D)
	@touch $@

.PRECIOUS: %/.DIR

.PHONY: all clean print libams amsInitHandler install

clean:
	rm -rf $(build_dir)/src $(build_dir)/examples $(build_dir)/tests $(BINDIR) $(LIBDIR)

etags:
	etags *.c *.h

# make print VAR=the-variable
print:
	@echo $($(VAR))

-include $(ALL_SRC.o:%.o=%.d)
