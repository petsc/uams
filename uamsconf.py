def mkdir_p(path):
    import os
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == os.errno.EEXIST:
            pass
        else: raise

def main(default=dict()):
    import argparse, os
    parser = argparse.ArgumentParser(description='Configure micro-Argonne Memory Snooper (uAMS)')
    parser.add_argument('--CC', help='Path to C compiler', default=default.get('CC','gcc'))
    parser.add_argument('--CFLAGS', help='Flags for C compiler', default=default.get('CFLAGS','-g3 -Wall -MMD -MP'))
    parser.add_argument('--LDFLAGS', help='Flags to pass to linker, e.g., -L<lib dir>', default=default.get('LDFLAGS',''))
    parser.add_argument('--LIBS', help='Extra libraries to link, e.g., -l<library>', default=default.get('LIBS','-lpthread -ldl -lm'))
    parser.add_argument('--prefix', help='Installation prefix', default=default.get('prefix','/usr/local'))
    parser.add_argument('--build-dir', help='Build directory', default=default.get('build_dir','build'))
    parser.add_argument('--enable-shared', help='Build shared libraries', default=default.get('enable_shared',''), action='store_true')
    parser.add_argument('--CFLAGS-SHARED', help='CFLAGS needed when compiling for shared libraries', default=default.get('CFLAGS_SHARED','-fPIC'))
    args = parser.parse_args()
    args.build_dir = os.path.abspath(args.build_dir)
    args.uams_dir = os.path.dirname(os.path.abspath(__file__))
    mkdir_p(args.build_dir)
    configure(args)

def configure(args):
    import sys,os
    with open(os.path.join(args.build_dir, 'Makefile'), 'w') as f:
        f.write(
            'CC := %(CC)s\n'
            'CFLAGS := %(CFLAGS)s\n'
            'CFLAGS_SHARED := %(CFLAGS_SHARED)s\n'
            'LDFLAGS := %(LDFLAGS)s\n'
            'LIBS := %(LIBS)s\n'
            'SHARED := %(enable_shared)s\n'
            'prefix := %(prefix)s\n'
            'libdir := $(prefix)/lib\n'
            'includedir := $(prefix)/include\n'
            'build_dir := %(build_dir)s\n'
            'include %(uams_dir)s/base.mk\n'
            % args.__dict__)
    reconfname = os.path.join(args.build_dir,'reconfigure.py')
    with open(reconfname, 'w') as f:
        f.write(
            '#!%(python)s\n'
            'import os, sys\n'
            'ARGS = %(args)r\n'
            "sys.path.insert(0, os.path.abspath(ARGS['uams_dir']))\n"
            'import uamsconf\n'
            'uamsconf.main(ARGS)\n'
            % dict(python=sys.executable, args=args.__dict__))
    os.chmod(reconfname,0o755)
    # Attempt to write Makefile in source directory to help out people who don't read directions
    try:
        with open(os.path.join(args.uams_dir, 'Makefile'), 'w') as f:
            f.write(
                'all lib install clean :\n'
                '\t$(MAKE) -C %(build_dir)s $@\n'
                '\n'
                '.PHONY: all lib insall clean\n'
                % dict(build_dir=args.build_dir))
    except IOError:             # It is okay if source directory is not writeable
        pass
    print('Configuration complete in: %s' % os.path.relpath(args.build_dir))
    print('To build: make -j3 -C %s' % os.path.relpath(args.build_dir))
