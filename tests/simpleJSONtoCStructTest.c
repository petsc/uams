#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ams-private/amsprivate.h"

/*includes amsprivate because we are testing private functions*/
int main(int argc,char **argv)
{ 
  /*Make up some data*/
  char        *final_JSON;
  char        *name                = "example";
  int         length               = 3;
  char        **stringData         = malloc(sizeof(char*));
  char        **stringDataCopy     = malloc(sizeof(char*));
  char        **stringDataArray    = malloc(length * sizeof(char*));
  char        **stringDataArrayCopy= malloc(length * sizeof(char*));
  int         *intData             = malloc(sizeof(int));
  int         *intDataArray        = malloc(length * sizeof(int)); 
  int         *intDataCopy         = malloc(sizeof(int));
  int         *intDataArrayCopy    = malloc(length * sizeof(int));
  double      *doubleData          = malloc(sizeof(double));
  double      *doubleDataArray     = malloc(length * sizeof(double));
  double      *doubleDataCopy      = malloc(sizeof(double));
  double      *doubleDataArrayCopy = malloc(length * sizeof(double));
  float       *floatData           = malloc(sizeof(float));
  float       *floatDataArray      = malloc(length * sizeof(float));
  float       *floatDataCopy       = malloc(sizeof(float));
  float       *floatDataArrayCopy  = malloc(length * sizeof(float));
  char        *charData            = malloc(sizeof(char));
  char        *charDataArray       = malloc(length * sizeof(char));
  char        *charDataCopy        = malloc(sizeof(char));
  char        *charDataArrayCopy   = malloc(length * sizeof(char));
  int         *boolData            = malloc(sizeof(int)),i = 0;
  int         *boolDataArray       = malloc(length * sizeof(int));
  int         *boolDataCopy        = malloc(sizeof(int));
  int         *boolDataArrayCopy   = malloc(length * sizeof(int));
  

  *boolData       = 1;
  *boolDataCopy   = 1;
  *intData        = 41;
  *intDataCopy    = 41;
  *doubleData     = 1.234567;
  *doubleDataCopy = 1.234567;
  *floatData      = 7.65432;
  *floatDataCopy  = 7.65432;
  *charData       = 'b';
  *charDataCopy   = 'b';
  *stringData     = strdup("string");
  *stringDataCopy = "string";

  for(i=0;i<length;i++){
    boolDataArray[i]       = 1;
    boolDataArrayCopy[i]   = 1;
    intDataArray[i]        = i + 12;
    intDataArrayCopy[i]    = i + 12;
    doubleDataArray[i]     = 27.7 + i;
    doubleDataArrayCopy[i] = doubleDataArray[i];
    floatDataArray[i]      = 13.3 + i;
    floatDataArrayCopy[i]  = 13.3 + i;
    charDataArray[i]       = 'b';
    charDataArrayCopy[i]   = 'b';
    stringDataArray[i]     = strdup("string");
    stringDataArrayCopy[i] = "string";
  }


  AMS_Initialize();
  /* Declare a memory */
  AMS_Memory amem;
  AMS_Field fld;

  

  /*Create a memory*/
  AMS_Memory_Create(name, &amem);

  /* add a field */
  AMS_New_Field(amem,"residual",boolData,1,AMS_WRITE,AMS_BOOLEAN);
  AMS_New_Field(amem,"residual2",stringData,1,AMS_WRITE,AMS_STRING);
  AMS_New_Field(amem,"residual3",doubleData,1,AMS_WRITE,AMS_DOUBLE);
  AMS_New_Field(amem,"residual4",intData,1,AMS_WRITE,AMS_INT);
  AMS_New_Field(amem,"residual5",floatData,1,AMS_WRITE,AMS_FLOAT);
  AMS_New_Field(amem,"residual6",charData,1,AMS_WRITE,AMS_CHAR);
  AMS_New_Field(amem,"residual7",boolDataArray,length,AMS_WRITE,AMS_BOOLEAN);
  AMS_New_Field(amem,"residual8",stringDataArray,length,AMS_WRITE,AMS_STRING);
  AMS_New_Field(amem,"residual9",doubleDataArray,length,AMS_WRITE,AMS_DOUBLE);
  AMS_New_Field(amem,"residual10",intDataArray,length,AMS_WRITE,AMS_INT);
  AMS_New_Field(amem,"residual11",floatDataArray,length,AMS_WRITE,AMS_FLOAT);
  AMS_New_Field(amem,"residual12",charDataArray,length,AMS_WRITE,AMS_CHAR);



  final_JSON = malloc(300 * amem->nb_fields + 80);
  /* parse the field */
  while(1){
  JSONParser_CStruct_to_JSON(final_JSON);
  cJSON_to_CStruct_Parser(final_JSON,NULL,NULL);
  }
  //printf("%s\n",final_JSON);
  JSONParser_CStruct_to_JSON(final_JSON);
  //printf("%s\n",final_JSON);
  cJSON_to_CStruct_Parser(final_JSON,NULL,NULL);
  JSONParser_CStruct_to_JSON(final_JSON);
  //printf("%s\n",final_JSON);

  fld = amem->fields;

  /*check each data mem`ber against its copy*/
  if(*(int*)fld->data != *boolDataCopy) printf("Single Boolean broken \n");
  fld = fld->next;
  if(strcmp(*(char**)fld->data,*stringDataCopy)) printf("Single String broken: field: %s copy: %s\n", *(char**)fld->data, *stringDataCopy);
  fld = fld->next;
  if(*(double*)fld->data != *doubleDataCopy) printf("Single Double broken \n");
  fld = fld->next;
  if(*(int*)fld->data != *intDataCopy) printf("Single Int broken \n");
  fld = fld->next;
  if(*(float*)fld->data != *floatDataCopy) printf("Single Float broken \n");
  fld = fld->next;
  if(*(char*)fld->data != *charDataCopy) printf("Single Char broken: field: %c copy: %c\n", *(char*)fld->data, *charDataCopy);
  fld = fld->next;




  for(i=0;i<length;i++){
    if(((int*)fld->data)[i] != boolDataArrayCopy[i]) printf("Array number: %d Boolean broken \n",i);
  }
  fld = fld->next;
  for(i=0;i<length;i++){
    if(strcmp(*(char**)fld->data,*stringDataCopy)) printf("Array number: %d String broken: field: %s copy: %s\n",i,*(char**)fld->data,*stringDataCopy);
  }
  fld = fld->next;
  for(i=0;i<length;i++){
    if(((double*)fld->data)[i] != doubleDataArrayCopy[i]) printf("Array number: %d Double broken. Field: %f copy: %f \n",i,((double*)fld->data)[i],doubleDataArrayCopy[i]);
  }  
  fld = fld->next;
  for(i=0;i<length;i++){
    if(((int*)fld->data)[i] != intDataArrayCopy[i]) printf("Array number: %d Int broken \n",i);
  } 
  fld = fld->next;
  for(i=0;i<length;i++){
    if(((float*)fld->data)[i] != floatDataArrayCopy[i]) printf("Array number: %d Float broken \n",i);
  }
  fld = fld->next;
  for(i=0;i<length;i++){
    if(((char*)fld->data)[i] != charDataArrayCopy[i]) printf("Array number: %d Char broken field: %c copy %c\n",i,((char*)fld->data)[i],charDataArrayCopy[i]);
  }



  AMS_Memory_Destroy(&amem);
  free(final_JSON);
  AMS_Finalize();

  /*free all the user data*/
  free(intData);
  free(intDataArray);
  free(intDataCopy);
  free(intDataArrayCopy);
  free(*stringData);
  free(stringData);
  free(stringDataCopy);
  free(stringDataArrayCopy);
  for(i=0;i<length;i++){
    free(stringDataArray[i]);
  }
  free(stringDataArray);
  free(doubleData);
  free(doubleDataArray);
  free(doubleDataArrayCopy);
  free(doubleDataCopy);
  free(floatData);
  free(floatDataArray);
  free(floatDataCopy);
  free(floatDataArrayCopy);
  free(charData);
  free(charDataArray);
  free(charDataCopy);
  free(charDataArrayCopy);
  free(boolData);
  free(boolDataArray);
  free(boolDataCopy);
  free(boolDataArrayCopy);

}
