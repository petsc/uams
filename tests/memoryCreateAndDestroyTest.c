#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ams.h"

int main(int argc,char **argv)
{ 

  AMS_Initialize();
  AMS_Memory amem;
  AMS_Memory amem2;
  AMS_Memory amem3;

  
  AMS_Memory_Create("example", &amem);
  AMS_Memory_Destroy(&amem);
  AMS_Memory_Create("example2", &amem2);
  AMS_Memory_Create("example3", &amem3);
  AMS_Memory_Destroy(&amem2);
  AMS_Memory_Destroy(&amem3);
  AMS_Finalize();


}
